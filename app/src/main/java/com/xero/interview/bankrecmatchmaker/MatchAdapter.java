package com.xero.interview.bankrecmatchmaker;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.ViewHolder> {
    private ISelect mISelect = null;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mainText;
        private TextView total;
        private TextView subtextLeft;
        private TextView subtextRight;

        public ViewHolder(View itemView) {
            super(itemView);
            mainText = itemView.findViewById(R.id.text_main);
            total = itemView.findViewById(R.id.text_total);
            subtextLeft = itemView.findViewById(R.id.text_sub_left);
            subtextRight = itemView.findViewById(R.id.text_sub_right);
        }

        public void bind(MatchItem matchItem) {
            mainText.setText(matchItem.getPaidTo());
            total.setText(Float.toString(matchItem.getTotal()));
            subtextLeft.setText(matchItem.getTransactionDate());
            subtextRight.setText(matchItem.getDocType());


        }

    }

    private List<MatchItem> matchItems;

    public MatchAdapter(List<MatchItem> matchItems) {
        this.matchItems = matchItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        CheckedListItem listItem = (CheckedListItem) layoutInflater.inflate(R.layout.list_item_match, parent, false);
        listItem.setSelect(mISelect);

        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MatchItem matchItem = matchItems.get(position);
        CheckedListItem listItem = (CheckedListItem) holder.itemView;
        listItem.setMatchItem(matchItem);
        if (mISelect != null) {
            ArrayList<Integer> autoSelectPositions = mISelect.getShouldAutoSelectedPositions();
            if (autoSelectPositions.size()>0 && autoSelectPositions.contains(position)) {
                listItem.setChecked(true);
            }
        } else {
            throw new RuntimeException("ISelect must not be null.");
        }
        holder.bind(matchItem);
    }

    @Override
    public int getItemCount() {
        return matchItems.size();
    }

    public void setSelect(ISelect select) {
        this.mISelect = select;
    }
}
