package com.xero.interview.bankrecmatchmaker;

import java.util.ArrayList;

/**
 * Created by anchen on 17/10/17.
 */

public interface ISelect {
    void onSelect(MatchItem item);

    void onUnSelect(MatchItem item);

    ArrayList<Integer> getShouldAutoSelectedPositions();
}
