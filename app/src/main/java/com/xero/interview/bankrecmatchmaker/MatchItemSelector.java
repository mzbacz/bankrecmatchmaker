package com.xero.interview.bankrecmatchmaker;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by anchen on 17/10/17.
 */

public class MatchItemSelector implements ISelect {
    private final TextView mMatchText;
    private final Context mContext;
    private float mTarget;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<MatchItem> mSortedMatchItems = null;
    private ArrayList<Integer> shouldAutoSelectedPositions = new ArrayList<Integer>();

    public MatchItemSelector(float target,
                             TextView matchText,
                             Context context,
                             RecyclerView.LayoutManager layoutManager,
                             List<MatchItem> sortedMatchItems

    ) {
        this.mTarget = target;
        this.mMatchText = matchText;
        this.mContext = context;
        this.mLayoutManager = layoutManager;
        this.mSortedMatchItems = sortedMatchItems;
        initAutoSelection();
    }

    @Override
    public void onSelect(MatchItem item) {
        this.mTarget = (this.mTarget * 100 - item.getTotal() * 100) / 100;
        mMatchText.setText(mContext.getString(R.string.select_matches, Math.round(mTarget)));
    }

    @Override
    public void onUnSelect(MatchItem item) {
        this.mTarget = (this.mTarget * 100 + item.getTotal() * 100) / 100;
        mMatchText.setText(mContext.getString(R.string.select_matches, Math.round(mTarget)));
    }

    private void setAutoSelectMatchTargetItemPositions() {
        if (mSortedMatchItems != null) {
            int[] totals = new int[mSortedMatchItems.size()];
            for (int i = 0; i < totals.length; i++) {
                totals[i] = Math.round(mSortedMatchItems.get(i).getTotal() * 100);
            }
            SubsetSum sumCalculator = new SubsetSum();
            sumCalculator.getAllSubsets(totals, totals.length, Math.round(mTarget * 100));
            ArrayList<ArrayList<Integer>> resultList = sumCalculator.getResultList();
            if (resultList.size() > 0) {
                //if there is multiple subset of sum always use the first one
                ArrayList<Integer> result = resultList.get(0);
                for (Integer res : result) {
                    for (MatchItem item : mSortedMatchItems) {
                        if (item.getTotal() * 100 == res)
                            shouldAutoSelectedPositions.add(item.getPosition());
                    }
                }


            }
        } else {
            throw new RuntimeException("sortedMatchItems must not be null.");
        }

    }

    private void initAutoSelection() {
        setAutoSelectMatchTargetItemPositions();
        if (shouldAutoSelectedPositions.size() > 0) {
            for (Integer position : shouldAutoSelectedPositions) {
                CheckedListItem view = (CheckedListItem) mLayoutManager.findViewByPosition(position);
                if (view != null)
                    view.setChecked(true);
                for (MatchItem item : mSortedMatchItems) {
                    if (item.getPosition() == position) {
                        this.mTarget = (this.mTarget * 100 - item.getTotal() * 100) / 100;
                        mMatchText.setText(mContext.getString(R.string.select_matches, Math.round(mTarget)));
                    }
                }
            }
        }
    }

    @Override
    public ArrayList<Integer> getShouldAutoSelectedPositions() {
        return shouldAutoSelectedPositions;
    }

    public void autoSelectMatchTargetItemPosition() {
        if (shouldAutoSelectedPositions.size() > 0) {
            for (Integer position : shouldAutoSelectedPositions) {
                CheckedListItem view = (CheckedListItem) mLayoutManager.findViewByPosition(position);
                if (view != null)
                    view.setChecked(true);
            }
        }

    }

}
