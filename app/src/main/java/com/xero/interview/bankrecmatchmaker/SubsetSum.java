package com.xero.interview.bankrecmatchmaker;

import java.util.ArrayList;

/**
 * Created by anchen on 17/10/17.
 */

public class SubsetSum {

    private boolean[][] dp;
    private ArrayList<ArrayList<Integer>> resultList = new ArrayList<ArrayList<Integer>>();


    // A recursive function to print all subsets with the
    // help of dp[][]. Vector p[] stores current subset.
    public void saveSubsetsRec(int arr[], int i, int sum,
                               ArrayList<Integer> p) {
        // If we reached end and sum is non-zero. We print
        // p[] only if arr[0] is equal to sun OR dp[0][sum]
        // is true.
        if (i == 0 && sum != 0 && dp[0][sum]) {
            p.add(arr[i]);
            resultList.add(p);
            return;
        }

        // If sum becomes 0
        if (i == 0 && sum == 0) {
            resultList.add(p);
            return;
        }

        // If given sum can be achieved after ignoring
        // current element.
        if (dp[i - 1][sum]) {
            // Create a new vector to store path
            ArrayList<Integer> b = new ArrayList<>();
            b.addAll(p);
            saveSubsetsRec(arr, i - 1, sum, b);
        }

        // If given sum can be achieved after considering
        // current element.
        if (sum >= arr[i] && dp[i - 1][sum - arr[i]]) {
            p.add(arr[i]);
            saveSubsetsRec(arr, i - 1, sum - arr[i], p);
        }
    }

    // Prints all subsets of arr[0..n-1] with sum 0.
    public void getAllSubsets(int arr[], int n, int sum) {
        if (n == 0 || sum < 0)
            return;

        // Sum 0 can always be achieved with 0 elements
        dp = new boolean[n][sum + 1];
        for (int i = 0; i < n; ++i) {
            dp[i][0] = true;
        }

        // Sum arr[0] can be achieved with single element
        if (arr[0] <= sum)
            dp[0][arr[0]] = true;

        // Fill rest of the entries in dp[][]
        for (int i = 1; i < n; ++i)
            for (int j = 0; j < sum + 1; ++j)
                dp[i][j] = (arr[i] <= j) ? (dp[i - 1][j] ||
                        dp[i - 1][j - arr[i]])
                        : dp[i - 1][j];
        if (dp[n - 1][sum] == false) {
            return;
        }

        // Now recursively traverse dp[][] to find all
        // paths from dp[n-1][sum]
        ArrayList<Integer> p = new ArrayList<>();
        saveSubsetsRec(arr, n - 1, sum, p);
    }

    public ArrayList<ArrayList<Integer>> getResultList() {
        return resultList;
    }

}
