package com.xero.interview.bankrecmatchmaker;

public class MatchItem {

    private final String paidTo;
    private final String transactionDate;
    private final float total;
    private final String docType;
    private int position;

    public MatchItem(String paidTo, String transactionDate, float total, String docType, int position) {
        this.paidTo = paidTo;
        this.transactionDate = transactionDate;
        this.total = total;
        this.docType = docType;
        this.position = position;
    }

    public String getPaidTo() {
        return paidTo;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public float getTotal() {
        return total;
    }

    public String getDocType() {
        return docType;
    }

    public int getPosition() {
        return position;
    }

}
