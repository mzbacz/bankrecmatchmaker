package com.xero.interview.bankrecmatchmaker;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FindMatchActivity extends AppCompatActivity {
    private List<MatchItem> mSortedMatchItems = null;
    public static final String TARGET_MATCH_VALUE = "com.xero.interview.target_match_value";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_match);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView matchText = findViewById(R.id.match_text);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_find_match);


        float target = getIntent().getFloatExtra(TARGET_MATCH_VALUE, 661.35f);
        matchText.setText(getString(R.string.select_matches, Math.round(target)));

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<MatchItem> items = buildMockData();
        mSortedMatchItems = new ArrayList(items);
        Collections.sort(mSortedMatchItems, new Comparator<MatchItem>() {
            @Override
            public int compare(MatchItem item1, MatchItem item2) {
                return Float.compare(item1.getTotal(), item2.getTotal());
            }
        });
        final MatchAdapter adapter = new MatchAdapter(items);
        adapter.setSelect(new MatchItemSelector(
                target, matchText,
                this,
                recyclerView.getLayoutManager(),
                mSortedMatchItems));
        recyclerView.setAdapter(adapter);
    }

    private List<MatchItem> buildMockData() {
        List<MatchItem> items = new ArrayList<>();
        items.add(new MatchItem("City Limousines", "30 Aug", 249.00f, "Sales Invoice", 0));
        items.add(new MatchItem("Ridgeway University", "12 Sep", 618.50f, "Sales Invoice", 1));
        items.add(new MatchItem("Cube Land", "22 Sep", 495.00f, "Sales Invoice", 2));
        items.add(new MatchItem("Bayside Club", "23 Sep", 234.00f, "Sales Invoice", 3));
        items.add(new MatchItem("SMART Agency", "12 Sep", 250f, "Sales Invoice", 4));
        items.add(new MatchItem("PowerDirect", "11 Sep", 108.60f, "Sales Invoice", 5));
        items.add(new MatchItem("PC Complete", "17 Sep", 216.99f, "Sales Invoice", 6));
        items.add(new MatchItem("Truxton Properties", "17 Sep", 181.25f, "Sales Invoice", 7));
        items.add(new MatchItem("MCO Cleaning Services", "17 Sep", 170.50f, "Sales Invoice", 8));
        items.add(new MatchItem("Gateway Motors", "18 Sep", 411.35f, "Sales Invoice", 9));
        return items;
    }

}
